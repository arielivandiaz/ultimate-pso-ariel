
import knapspack as knp
import pyswarmsHandler as psh
import numpy as np
from args import args


if __name__ == '__main__':
  
    args.fn = knp.mochila

    solX,t= psh.discrete_optimization(args)

    solPru = np.random.randint(2, size=(1, args.nro_items))
    
    for k in range(0, args.nro_items):
        solPru[0, k] = solX[k]


    [f, vio] = knp.mochila_res(solPru)
    #print(f)
    violations = np.count_nonzero(vio)
    if(np.count_nonzero(vio)): print("VIOLATED")

    print(f)
    print(violations)
    print(vio)
    print(t)

