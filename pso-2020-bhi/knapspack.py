
from args import args
USE_JAX = 0
if (USE_JAX):
    import jax.numpy as np
    from jax import random
    from jax import grad, jit
else:
    import numpy as np


def generar_instancia():

    if (USE_JAX):
        key = random.PRNGKey(0)

    alfa = 0.75

    pv = np.zeros(args.nro_items)
    rs = np.zeros((args.nro_items, args.nro_restricciones))
    bs = np.zeros(args.nro_restricciones)

    if (USE_JAX):
        rs = random.randint(
            key, (args.nro_restricciones, args.nro_items), 0, 1000)
        q = random.uniform(key, (args.nro_items, 1), None)
    else:
        rs = np.random.randint(1000, size=(
            args.nro_restricciones, args.nro_items))
        q = np.random.random_sample((args.nro_items))

    bs = alfa * np.sum(rs, axis=1)

    pv = (1 / args.nro_restricciones) * np.sum(rs, axis=0)
    pv += 500*q

    return pv, rs, bs

pv, rs, bs= generar_instancia()


def mochila(X):
    P = len(X)
    N = len(X[0])
    M = args.nro_restricciones

    Peso = np.sum(pv, axis=0)

    PR = np.zeros(P)
    G = np.zeros((P, M))
    VIO = np.zeros((P, M))
    PNLTY = np.zeros(P)
    FI = np.zeros(P)

    for i in range(P):
        PR[i] = 0.0
        for j in range(N):
            PR[i] = pv[j] * X[i, j] + PR[i]  # Funcion objetivo

        for k in range(M):
            G[i, k] = 0.0
            for j in range(N):
                G[i, k] = rs[k, j] * X[i, j] + G[i, k]  # Restricciones
            VIO[i, k] = G[i, k] - bs[k]  # Violaciones

        PNLTY[i] = 0.0

        for k in range(M):
            if (VIO[i, k] <= 0.0):
                VIO[i, k] = 0.0
            PNLTY[i] = VIO[i, k] + PNLTY[i]  # Penalidad

        FI[i] = PR[i] - Peso * PNLTY[i]  # Func.Obj.con penalizacion

    return -FI


def mochila_res(X):

    P = len(X)
    N = len(X[0])
    M = args.nro_restricciones

    Peso = np.sum(pv, axis=0)

    PR = np.zeros(P)
    G = np.zeros((P, M))
    VIO = np.zeros((P, M))
    PNLTY = np.zeros(P)
    FI = np.zeros(P)

    for i in range(P):
        PR[i] = 0.0
        for j in range(N):
            PR[i] = pv[j] * X[i, j] + PR[i]  # Funcion objetivo

        for k in range(M):
            G[i, k] = 0.0
            for j in range(N):
                G[i, k] = rs[k, j] * X[i, j] + G[i, k]  # Restricciones
            VIO[i, k] = G[i, k] - bs[k]  # Violaciones

        PNLTY[i] = 0.0

        for k in range(M):
            if (VIO[i, k] <= 0.0):
                VIO[i, k] = 0.0
            PNLTY[i] = VIO[i, k] + PNLTY[i]  # Penalidad

        FI[i] = PR[i] - Peso * PNLTY[i]  # Func.Obj.con penalizacion

    return FI, VIO



