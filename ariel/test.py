

import time

import numpy as np
# Import PySwarms
import pyswarms as ps
from numpy import savetxt, loadtxt


# Referencia:
# Solving the Multidimensional Knapsack Problemusing a CUDA accelerated PSO
# Drahoslav Zan and Jiri Jaros
# 2014 IEEE Congress on Evolutionary Computation (CEC)
# July 6-11, 2014, Beijing, China


def mochila(X):
    P = len(X)
    N = len(X[0])
    M = nro_restricciones
    # print(P,N,M)

    Peso = np.sum(pv_file, axis=0)

    PR = np.zeros(P)
    G = np.zeros((P, M))
    VIO = np.zeros((P, M))
    PNLTY = np.zeros(P)
    FI = np.zeros(P)

    for i in range(P):
        PR[i] = 0.0
        for j in range(N):
            PR[i] = pv_file[j] * X[i, j] + PR[i]  # Funcion objetivo

        for k in range(M):
            G[i, k] = 0.0
            for j in range(N):
                G[i, k] = rs_file[k, j] * X[i, j] + G[i, k]  # Restricciones
            VIO[i, k] = G[i, k] - bs_file[k]  # Violaciones

        PNLTY[i] = 0.0

        for k in range(M):
            if (VIO[i, k] <= 0.0):
                VIO[i, k] = 0.0
            PNLTY[i] = VIO[i, k] + PNLTY[i]  # Penalidad

        FI[i] = PR[i] - Peso * PNLTY[i]  # Func.Obj.con penalizacion

    return -FI


def mochila_res(X):
    # esta funcion es la misma que la usa pyswarms pero develve la funcion objetivo y el vector violacion de restricciones

    P = len(X)
    N = len(X[0])
    M = nro_restricciones

    Peso = np.sum(pv_file, axis=0)

    PR = np.zeros(P)
    G = np.zeros((P, M))
    VIO = np.zeros((P, M))
    PNLTY = np.zeros(P)
    FI = np.zeros(P)

    for i in range(P):
        PR[i] = 0.0
        for j in range(N):
            PR[i] = pv_file[j] * X[i, j] + PR[i]  # Funcion objetivo

        for k in range(M):
            G[i, k] = 0.0
            for j in range(N):
                G[i, k] = rs_file[k, j] * X[i, j] + G[i, k]  # Restricciones
            VIO[i, k] = G[i, k] - bs_file[k]  # Violaciones

        PNLTY[i] = 0.0

        for k in range(M):
            if (VIO[i, k] <= 0.0):
                VIO[i, k] = 0.0
            PNLTY[i] = VIO[i, k] + PNLTY[i]  # Penalidad

        FI[i] = PR[i] - Peso * PNLTY[i]  # Func.Obj.con penalizacion

    return FI, VIO


def test_mochila(swarm_size):
    X = np.random.randint(2, size=(swarm_size, nro_items))

    print(X)

    result = mochila(X)
    print("result vector")
    print(result)


def generar_instancia(nro_items, nro_restricciones, alfa=0.75):
    rs = np.random.randint(1000, size=(nro_restricciones, nro_items))
    bs = alfa * np.sum(rs, axis=1)
    q = np.random.random_sample((nro_items))

    pv = (1 / nro_restricciones) * np.sum(rs, axis=0)
    for j in range(0, nro_items - 1):
        pv[j] = pv[j] + 500 * q[j]

    return pv, rs, bs


def imprimir_solucion(solX):
    solPru = np.random.randint(2, size=(1, nro_items))
    for k in range(0, nro_items):
        solPru[0, k] = solX[k]

    print(solX)
    print(solPru)

    [f, vio] = mochila_res(solPru)
    print(f)
    print(vio)


def activation(x, function=1):
    if (function == 1):  # Sigmoid
        return 1 / (1 + np.exp(-x))
    elif (function == 2):  # Softmax
        expo = np.exp(x)
        expo_sum = np.sum(np.exp(x))
        return expo / expo_sum
    elif (function == 3):  # ReLu
        return np.maximum(0, x)


def toDiscrete(x):
    return np.where(x < 0.5, 0, 1)


def convert_to_discrete(x):
    x_sigmoid = 1 / (1 + np.exp(-x))
    discrete_position = np.where(x_sigmoid < 0.5, 0, 1)
    return discrete_position


def calculate_velocity(w, particles_velocity, c1, c2, r1, r2, best_particle_position,
                       particles_position, best_global_position):
    inertia = w * particles_velocity
    best_particle_pos_component = r1 * (best_particle_position - particles_position)
    best_global_pos_component = r2 * (best_global_position - particles_position)

    new_velocity = inertia + c1 * best_particle_pos_component + c2 * best_global_pos_component
    return new_velocity

def calculate_best_position(objective_values, best_particle_cost, particles_position, best_particle_position, particles,
                            dimensions):
    bests = np.less(objective_values, best_particle_cost)
    reshape = np.reshape(bests, np.array([particles, 1]))
    bests_reshape = np.broadcast_to(reshape, np.array([particles, dimensions]))
    pos = np.where(bests_reshape, particles_position, best_particle_position)
    return pos


def runDiscretePSO(user_options, algorithm_options):
    # For each particle, initialize position and velocity
    particles_position = np.random.uniform(-1, 1, (algorithm_options['particles'], algorithm_options['dimensions']))
    particles_velocity = np.random.uniform(-1, 1, (algorithm_options['particles'], algorithm_options['dimensions']))

    # particles_position = convert_to_discrete(particles_position)
    particles_position = toDiscrete(activation(particles_position))

    k = 0
    particles = algorithm_options['particles']
    dimensions = algorithm_options['dimensions']
    best_global = None  # Best swarm cost
    best_global_position = np.empty((particles, dimensions))  # Best swarm position
    best_particle_position = particles_position
    best_particle_cost = algorithm_options['objective'](best_particle_position)  # obj_fuction(best_particle_position)

    while k < algorithm_options['iterations']:
        objective_values = algorithm_options['objective'](best_particle_position)  # obj_fuction(particles_position)
        best_index = np.argmin(objective_values)
        best_value = objective_values[best_index]

        # 150 x 20 !!!
        best_particle_position = calculate_best_position(objective_values, best_particle_cost, particles_position,
                                                         best_particle_position, particles, dimensions)

        if best_global is None or best_value < best_global:
            # Update best swarm cost and position
            best_global = best_value
            best_global_position = particles_position[best_index]

        r1 = np.random.uniform(-1, 1, (algorithm_options['particles'], algorithm_options['dimensions']))
        r2 = np.random.uniform(-1, 1, (algorithm_options['particles'], algorithm_options['dimensions']))

        particles_velocity = calculate_velocity(user_options['w'], particles_velocity, user_options['c1'],
                                                user_options['c2'], r1, r2, best_particle_position,
                                                particles_position, best_global_position)

        new_particle_positions = particles_position + particles_velocity
        particles_position = convert_to_discrete(new_particle_positions)
        # particles_position = toDiscrete(activation((calculate_position(particles_position, particles_velocity))))

        best_cost_yet_found = np.min(best_particle_cost)
        relative_measure = algorithm_options['ftol'] * (1 + np.abs(best_cost_yet_found))
        if ( np.any(np.abs(best_particle_cost - best_cost_yet_found) < relative_measure) ):
            break

        k += 1

    return best_global, best_global_position


def solucion_nuestra(num_particles, iterations, dim, ftol, options):
    user_options = {'c1': options['c1'], 'c2': options['c1'], 'w': options['w']}
    algorithm_options = {'particles': num_particles, 'dimensions': dim,
                         'iterations': iterations, 'objective': mochila, 'ftol': ftol}

    # Perform optimization
    start = time.time()
    solFO, solX = runDiscretePSO(user_options, algorithm_options)
    wall_time = time.time() - start

    # print resutls
    print("wall_time nuestro: {:0.2f}ms".format(1000 * wall_time))
    imprimir_solucion(solX)


def solucion_pyswarm(num_particles, iteraciones, dimensiones, ftol, options):
    optimizer = ps.discrete.binary.BinaryPSO(n_particles=num_particles,
                                             dimensions=dimensiones,
                                             options=options,
                                             init_pos=None,
                                             velocity_clamp=None,
                                             vh_strategy='unmodified', ftol=ftol)

    # Perform optimization
    start = time.time()
    [solFO, solX] = optimizer.optimize(mochila, iters=iteraciones)
    wall_time = time.time() - start

    # print resutls
    print("wall_time pycharm: {:0.2f}ms".format(1000 * wall_time))
    imprimir_solucion(solX)


def comparar_solciones():
    # function particular variables
    global pv_file
    global bs_file
    global rs_file
    global nro_items
    global nro_restricciones

    nro_items = 20
    nro_restricciones = 5
    [pv_temp, rs_temp, bs_temp] = generar_instancia(nro_items, nro_restricciones)

    # save to csv file
    savetxt('pv.csv', pv_temp, delimiter=',')
    savetxt('rs.csv', rs_temp, delimiter=',')
    savetxt('bs.csv', bs_temp, delimiter=',')

    # load data from files
    pv_file = loadtxt('pv.csv', delimiter=',')
    rs_file = loadtxt('rs.csv', delimiter=',')
    bs_file = loadtxt('bs.csv', delimiter=',')

    dimensiones = nro_items
    iteraciones = 100
    num_particles = 100
    ftol = .5

    solucion_pyswarm(num_particles, iteraciones, dimensiones, ftol, options={'c1': 0.5, 'c2': 0.3, 'w': 0.9, 'k': num_particles, 'p': 1})
    solucion_nuestra(num_particles, iteraciones, dimensiones, ftol, options={'c1': 0.5, 'c2': 0.3, 'w': 0.9, 'k': num_particles, 'p': 1})


if __name__ == '__main__':
    comparar_solciones()